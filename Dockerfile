FROM python:alpine

WORKDIR /app

COPY requirements.txt /app
RUN pip install -r requirements.txt

COPY docksi.py /app

VOLUME ["/.local/docksi-bin"]

ENTRYPOINT ["/app/docksi.py"]
